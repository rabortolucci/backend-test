# Java Code Challenge - Versão 1.0

### Passos para configurar o ambiente

```
* Projeto Maven feito no IntelliJ utilizando Spring Boot
* Para rodar a aplicação, favor executar a main classe: BackendTestApplication
* Para rodar os testes unitários, favor executar: BackendTestApplicationTests

```
### Passos para testar a API
```
* Para acessar os serviços, favor seguir os passos descritos no PDF: Instrucoes_testar_API.pdf
```

